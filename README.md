# minchash

minchash is meant to be an incremental multi-set hashing tool. Although it is not currently a true hash since resulting hashes can vary in length.

## Future

These are a few ideas for future modifications:

- Provide a list of options to choose from for a "P" value (e.g. 31, 2971215073, 11400714819323198485)
- Perhaps default to one of the numbers in the list based off of architecture (e.g. 32-bit = 2971215073, 64-bit = 11400714819323198485)
- alter minchash to produce hashes of a static size (by allowing addition and multiplcation operations to overflow)